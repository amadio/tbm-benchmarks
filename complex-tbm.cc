#include <algorithm>
#include <cstdlib>
#include <random>
#include <thread>
#include <vector>

#include "ROOT/TBufferMerger.hxx"
#include "TROOT.h"
#include "TTree.h"
#include "Compression.h"

#include "event.h"

using namespace ROOT::Experimental;

int main(int argc, char **argv)
{
   gROOT->SetBatch();

   size_t nWorkers = 1;
   size_t Compress = 0;
   size_t nBranches = 10;
   size_t nEventsPerBranch = 10;
   size_t AutoSave = 0;
   size_t nEntries = 100000;

   if (argc >= 2) nWorkers         = strtoll(argv[1], nullptr, 10);
   if (argc >= 3) Compress         = strtoll(argv[2], nullptr, 10);
   if (argc >= 4) nBranches        = strtoll(argv[3], nullptr, 10);
   if (argc >= 5) nEventsPerBranch = strtoll(argv[4], nullptr, 10);
   if (argc >= 6) AutoSave         = strtoll(argv[5], nullptr, 10);
   if (argc >= 7) nEntries         = strtoll(argv[6], nullptr, 10);

   size_t nEntriesPerWorker = nEntries/nWorkers;

   ROOT::EnableThreadSafety();

   switch(Compress) {
     case 3:
       Compress = (size_t) ROOT::CompressionSettings(ROOT::kLZMA, 9);
       break;
     case 2:
       Compress = (size_t) ROOT::CompressionSettings(ROOT::kZLIB, 1);
       break;
     case 1:
       Compress = (size_t) ROOT::CompressionSettings(ROOT::kLZ4, 4);
       break;
     case 0:
     default:
       Compress = 0ul; /* uncompressed */
   }

   TBufferMerger merger("complex-tbm.root", "recreate", Compress);

   if (AutoSave)
      merger.SetAutoSave(AutoSave);

   auto random = [=, &merger](size_t seed) {
      std::default_random_engine g(seed);
      std::uniform_real_distribution<double> dist(0.0, 1.0);

      auto f = merger.GetFile();
      auto t = new TTree("complex", "complex");
      t->ResetBit(kMustCleanup);
      t->SetAutoFlush(-32*1024*1024);

      std::vector<std::vector<Event>> events(nBranches);

      for (size_t i = 0; i < nBranches; ++i) {
         std::vector<Event> vec(nEventsPerBranch);
         events[i] = vec;
         std::string brname = "ev_" + std::to_string(i);
         t->Branch(brname.c_str(), &events[i]);
      }

      long entries = 0;
      for (size_t i = 0; i < nEntriesPerWorker; ++i) {
         for (auto& ev : events)
            for (auto& e : ev)
               e = Event(dist(g));

         t->Fill();
         ++entries;
         auto atflush = t->GetAutoFlush();
         if (entries == atflush) {
            entries = 0;
            f->Write();
         }
      }

      f->Write();
   };

   std::vector<std::thread> workers;
   for (size_t i = 1; i < nWorkers; ++i)
      workers.emplace_back(random, i);

   random(0);

   for (auto&& worker : workers)
      worker.join();

   return 0;
}
