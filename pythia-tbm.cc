#include <thread>
#include <vector>

#include "Pythia8/Pythia.h"
#include "ROOT/TBufferMerger.hxx"
#include "TROOT.h"
#include "TTree.h"

using namespace ROOT::Experimental;

int main(int argc, char **argv) {
   size_t nWorkers  = 8;
   size_t nEvents   = 8192;
   long FileCacheMB = 64;

   std::string filename("pythia-tbm.root");

   if (argc >= 2) nWorkers    = std::atoi(argv[1]);
   if (argc >= 3) nEvents     = std::atoi(argv[2]);
   if (argc >= 4) FileCacheMB = std::atoi(argv[3]);
   if (argc >= 5) filename    = argv[4];

   size_t nEventsPerWorker = nEvents/nWorkers;

   FileCacheMB *= 1024*1024; // convert to bytes

   gROOT->SetBatch();

   if (nWorkers > 1)
      ROOT::EnableImplicitMT(nWorkers);

   TBufferMerger merger(filename.c_str(), "recreate");

   auto pythia_gen = [=, &merger]() {
      Pythia8::Pythia pythia;
      pythia.readString("Print:quiet = on");
      pythia.readString("HardQCD:all = on");
      pythia.readString("PhaseSpace:pTHatMin = 20.");
      pythia.readString("Beams:eCM = 14000.");
      pythia.init();

      auto f = merger.GetFile();
      Pythia8::Event *e = &pythia.event;
      auto t = new TTree("pythia", "pythia");
      t->ResetBit(kMustCleanup);

      t->Branch("event", &e);

      for (size_t n = 0; n < nEventsPerWorker; ++n) {
         while (!pythia.next());

         t->Fill();

         if (f->GetBytesWritten() > FileCacheMB)
            f->Write();
      }

      f->Write();
   };

   std::vector<std::thread> workers;
   for (size_t i = 0; i < nWorkers; ++i)
      workers.emplace_back(pythia_gen);

   for (auto&& worker : workers)
      worker.join();

   return 0;
}
