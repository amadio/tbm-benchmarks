#include <cstdlib>
#include <thread>
#include <vector>

#include "Pythia8/Pythia.h"
#include "ROOT/TBufferMerger.hxx"
#include "TFile.h"
#include "TROOT.h"
#include "TTree.h"

using namespace ROOT::Experimental;

int main(int argc, char **argv) {
   size_t nWorkers  = 2;
   size_t nEvents   = 1000;

   if (argc >= 2) nWorkers = std::atoi(argv[1]);
   if (argc >= 3) nEvents  = std::atoi(argv[2]);

   size_t nEventsPerWorker = nEvents/nWorkers;

   gROOT->SetBatch();
   ROOT::EnableThreadSafety();

   auto pythia_gen = [=]() {
      Pythia8::Pythia pythia;
      pythia.readString("Print:quiet = on");
      pythia.readString("HardQCD:all = on");
      pythia.readString("PhaseSpace:pTHatMin = 20.");
      pythia.readString("Beams:eCM = 14000.");
      pythia.init();

      std::string filename = std::string(std::tmpnam(nullptr)) + ".root";

      auto f = TFile::Open(filename.c_str(), "recreate");
      Pythia8::Event *e = &pythia.event;
      auto t = new TTree("pythia", "pythia");

      t->Branch("event", &e);

      for (size_t n = 0; n < nEventsPerWorker; ++n) {
         while (!pythia.next());

         t->Fill();
      }

      f->Write();
      f->Close();
      delete f;
   };

   std::vector<std::thread> workers;
   for (size_t i = 0; i < nWorkers; ++i)
      workers.emplace_back(pythia_gen);

   for (auto&& worker : workers)
      worker.join();

   return 0;
}
