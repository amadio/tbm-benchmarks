#include <thread>
#include <vector>

#include "Pythia8/Pythia.h"

int main(int argc, char **argv)
{
   size_t nWorkers = 2;
   size_t nEvents  = 1000;

   if (argc >= 2) nWorkers = std::atoi(argv[1]);
   if (argc >= 3) nEvents  = std::atoi(argv[2]);

   auto nEventsPerWorker = nEvents/nWorkers;

   auto pythia_gen = [nEventsPerWorker]() {
      Pythia8::Pythia pythia;
      pythia.readString("Print:quiet = on");
      pythia.readString("HardQCD:all = on");
      pythia.readString("PhaseSpace:pTHatMin = 20.");
      pythia.readString("Beams:eCM = 14000.");
      pythia.init();

      for (size_t n = 0; n < nEventsPerWorker; ++n)
         while (!pythia.next()) continue;
   };

   std::vector<std::thread> workers;
   for (size_t i = 0; i < nWorkers; ++i)
      workers.emplace_back(pythia_gen);

   for (auto&& worker : workers) 
      worker.join();

   return 0;
}
