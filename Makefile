CC=gcc
CXX=g++

CPPFLAGS=-Iinclude
CXXFLAGS=$(CPPFLAGS) -O3 -march=native

ROOTSYS=$(shell root-config --prefix)

ROOTCONFIG=$(ROOTSYS)/bin/root-config
ROOTCLING=$(ROOTSYS)/bin/rootcling
ROOTFLAGS=$(shell $(ROOTCONFIG) --cflags)
ROOTLDFLAGS=-Wl,-rpath=$(ROOTSYS)/lib
ROOTLIBS=$(ROOTLDFLAGS) $(shell $(ROOTCONFIG) --libs)

all: random complex

debug: all
debug: CXXFLAGS += -DDEBUG -g

pythia: pythia-files pythia-gen pythia-tbm pythia-tdf
pythia: LDFLAGS += $(shell pythia8-config --libs)
pythia: CXXFLAGS += $(shell pythia8-config --cxxflags)
pythia: CXXFLAGS += -Wno-delete-non-virtual-dtor

random: random-tbm random-tdf

pythia-dict.cc: include/pythia-dict.h include/pythia-linkdef.h
	$(ROOTCLING) -f $@ $(ROOTFLAGS) $(shell pythia8-config --cflags) $^

pythia-gen: pythia-gen.cc
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

pythia-files: pythia-dict.cc pythia-files.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

pythia-tbm: pythia-dict.cc pythia-tbm.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

pythia-tdf: pythia-dict.cc pythia-tdf.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

random-tdf: random-tdf.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

random-tbm: random-tbm.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

event-dict.cc: include/event.h include/event-linkdef.h
	$(ROOTCLING) -f $@ $(ROOTFLAGS) $^

complex: complex-tbm

complex-tbm: event-dict.cc complex-tbm.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

simple: simple.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

root-ls: root-ls.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

root-merge: root-merge.cc
	$(CXX) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) $(LDFLAGS)

clean:
	$(RM) -rf pythia-dict.cc *.pcm
	$(RM) -rf pythia-{files,gen,tbm,tdf}
	$(RM) -rf random-{tbm,tdf} simple
	$(RM) -rf complex-tbm event-dict.cc
	$(RM) -rf root-{ls,merge}
