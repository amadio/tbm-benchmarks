#ifndef EVENT_H
#define EVENT_H

#include <cstddef>
#include <cstdint>
#include <random>
#include <vector>

enum class Particle {
   Electron, Positron, Proton, Neutron, Unknown
};

struct Vector3D { double x, y, z; };

struct Event {
   Event(size_t seed = 0)
   {
      std::default_random_engine g(seed);
      std::uniform_real_distribution<double> dist(0.0, 1.0);

      p1.x = dist(g); p1.y = dist(g); p1.z = dist(g);
      p2.x = dist(g); p2.y = dist(g); p2.z = dist(g);
      p3.x = dist(g); p3.y = dist(g); p3.z = dist(g);

      E1 = dist(g); E2 = dist(g); E3 = dist(g);

      std::uniform_int_distribution<int> idist(0, 4);

      type1 = GetParticle(idist(g));
      type2 = GetParticle(idist(g));
   }

   Vector3D p1, p2, p3;
   double E1, E2, E3;
   Particle type1, type2;

   private:

   Particle GetParticle(int i)
   {
      static constexpr Particle types[] =
         {
            Particle::Electron,
            Particle::Positron,
            Particle::Proton,
            Particle::Neutron,
            Particle::Unknown
         };
      return types[i % 4];
   }
};

#endif
