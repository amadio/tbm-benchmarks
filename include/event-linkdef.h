#ifdef __CLING__

#pragma link C++ class Event+;
#pragma link C++ class Vector3D+;
#pragma link C++ class std::vector<Event>+;
#pragma link C++ class std::vector<std::vector<Event>>+;

#endif
