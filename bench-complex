#!/bin/bash

names=(None LZ4 ZLIB LZMA)

time="/usr/bin/time -f \"%e %U %S %K %M\""

THREADS=(1 2 4 6 8 10 12 14 16 18 20 24 28 32 36 40 48 56 64 72 80 96 112 128 144 160 192 224 256)

function clear_line() { printf "\r%${COLUMNS}s" " "; }

function average() {
	awk -f - $@ <<-EOF
	{
		for (i = 1; i <= NF; i++)
			sum[i] += \$i;
	}

	END {
		for (i = 1; i < NF; i++)
			printf "%.2f ", sum[i]/NR;
		printf "%.2f\n", sum[NF]/NR;
	}
	EOF
}

function run_benchmarks() {
	local prog=${1:-complex-tbm}
	local comp=${2:-0}
	local brch=${3:-10}
	local evts=${4:-10}
	local runs=${5:-3}

	# clean up old files
	rm -f ${prog}.{dat,log,out}

	date

	for nthr in ${THREADS[@]}; do
		# run only up to hardware concurrency
		[[ ${nthr} -gt $(nproc) ]] && break

		rm -f ${prog}.log

		for run in $(seq $runs); do
			clear_line
			echo -ne "\rRunning Benchmark: $prog $nthr $comp $brch $evts, run ${run}"
			eval ${time} -a -o ${prog}.log "$prog $nthr $comp $brch $evts >/dev/null"
		done

		# compute average of all jobs
		average ${prog}.log >> ${prog}.out
	done

	# prepend number of threads to the output
	paste -d" " <(echo ${THREADS[@]} | xargs -n1) ${prog}.out |
	head -n $(wc -l ${prog}.out | cut -d" " -f1) >| ${prog}-${names[${compression}]}.dat
	rm -f ${prog}.{log,out} *.root
	echo
}

run_benchmarks $@
