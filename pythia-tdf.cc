#include "Pythia8/Pythia.h"

#include "ROOT/RDataFrame.hxx"
#include "ROOT/TSpinMutex.hxx"
#include "TFile.h"
#include "TTree.h"

class TSlotStack {
   private:
      unsigned int fCursor;
      std::vector<unsigned int> fBuf;
      ROOT::TSpinMutex fMutex;

   public:
      TSlotStack() = delete;
      TSlotStack(unsigned int size) : fCursor(size), fBuf(size) {
         std::iota(fBuf.begin(), fBuf.end(), 0U);
      }
      void Push(unsigned int slotNumber);
      unsigned int Pop();
};

void TSlotStack::Push(unsigned int slotNumber) {
   std::lock_guard<ROOT::TSpinMutex> guard(fMutex);
   fBuf[fCursor++] = slotNumber;
}

unsigned int TSlotStack::Pop() {
   std::lock_guard<ROOT::TSpinMutex> guard(fMutex);
   return fBuf[--fCursor];
}

int main(int argc, char **argv)
{
   gROOT->SetBatch();

   size_t nWorkers  = 2;
   size_t nEvents   = 1000;

   std::string filename("pythia-tdf.root");

   if (argc >= 2) nWorkers = std::atoi(argv[1]);
   if (argc >= 3) nEvents  = std::atoi(argv[2]);
   if (argc >= 4) filename = argv[3];

   if (nWorkers > 1)
      ROOT::EnableImplicitMT(nWorkers);

   std::vector<Pythia8::Pythia> pythias(nWorkers);
   for (auto& pythia : pythias) {
      pythia.readString("Print:quiet = on");
      pythia.readString("HardQCD:all = on");
      pythia.readString("PhaseSpace:pTHatMin = 20.");
      pythia.readString("Beams:eCM = 14000.");
      pythia.init();
   }

   TSlotStack sstack(nWorkers);
   auto pythia_gen = [&]() {
      auto slot = sstack.Pop();
      auto &pythia = pythias[slot];
      while (!pythia.next());
      sstack.Push(slot);
      return &pythia.event;
   };

   ROOT::RDataFrame tdf(nEvents);
   tdf.Define("event", pythia_gen)
      .Snapshot<Pythia8::Event*>("tree", filename.c_str(), {"event"});

   return 0;
}
