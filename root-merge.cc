#include <iostream>
#include <cstdio>
#include <vector>

#include "TH1.h"
#include "TKey.h"
#include "TMemFile.h"
#include "TTree.h"

TH1*        merge(TH1*, TH1*);
TTree*      merge(TTree*, TTree*);

TObject*    merge(TKey*, TKey*);
TObject*    merge(TObject*, TObject*);

TDirectory* merge(TDirectory*, TKey*);
TDirectory* merge(TDirectory*, TObject*);
TDirectory* merge(TDirectory*, TDirectory*);

TH1* merge(TH1 *dst, TH1 *src)
{
   fprintf(stderr, "merge<Hist>()\n");

   if (!src || !dst || src == dst)
      return dst;

   dst->Add(src);

   return dst;
}

TTree* merge(TTree *dst, TTree *src)
{
   fprintf(stderr, "merge<Tree>()\n");

   if (!src || !dst || src == dst)
      return dst;

   if (src->GetEntries() == 0)
      return dst;

   dst->CopyAddresses(src);
   dst->CopyEntries(src, -1, "fast");

   return dst;
}

TObject* merge(TKey *dst, TKey *src)
{
   fprintf(stderr, "merge<Key>() -> ");

   if (!src || !dst || src == dst)
      return dst;

   return merge(dst->ReadObj(), src->ReadObj());
}

TDirectory* merge(TDirectory *dst, TKey* src)
{
   fprintf(stderr, "merge<Dir, Key>() -> ");

   if (!src || !dst)
      return dst;

   return merge(dst, src->ReadObj());
}

TDirectory* merge(TDirectory *dst, TObject* src)
{
   fprintf(stderr, "merge<Dir, Obj>\n");

   if (!src || !dst)
      return dst;

   TDirectory::TContext c;

   dst->cd();
   src->Write(src->GetName());
   dst->SaveSelf(true);

   return dst;
}

TDirectory* merge(TDirectory *dst, TDirectory* src)
{
   fprintf(stderr, "merge<Dir, Dir>\n");

   if (!src || !dst || src == dst)
      return dst;

   std::map<std::string, TObject*> object_map;

   /* load objects from memory */
   for (TObject *obj : *(dst->GetList()))
      object_map[obj->GetName()] = obj;

   /* load objects from disk */
   for (TObject *obj : *(dst->GetListOfKeys()))
      if (TKey *key = dynamic_cast<TKey*>(obj))
         object_map[key->GetName()] = key->ReadObj();

   for (auto src_key : *(src->GetListOfKeys())) {
      std::string key_name = src_key->GetName();
      auto pair = object_map.find(key_name);
      if (pair != object_map.end()) {
         auto dst_key = pair->second;
         if (strcmp(dst_key->GetName(), src_key->GetName()) == 0) {
            TObject* src_obj = dynamic_cast<TObject*>(src_key);
            TObject* dst_obj = dynamic_cast<TObject*>(dst_key);
            if (auto result = merge(dst_obj, src_obj))
               object_map[key_name] = result;
         }
      } else {
         if (TObject *object = dynamic_cast<TKey*>(src_key)->ReadObj())
            object_map[src_key->GetName()] = object;
      }
   }

   TDirectory::TContext c;

   dst->cd();

   for (auto& pair : object_map)
      if (TObject *object = pair.second)
         object->Write(pair.first.c_str());

   dst->SaveSelf(true);

   return dst;
}

TObject* merge(TObject *dst, TObject *src)
{
   fprintf(stderr, "merge<Object>() -> ");

   if (!src || !dst || src == dst)
      return dst;

   fprintf(stderr, "TKey -> ");
   if (TKey *key_src = dynamic_cast<TKey*>(src))
      if (TKey *key_dst = dynamic_cast<TKey*>(dst))
       return merge(key_dst, key_src);

   fprintf(stderr, "TTree -> ");
   if (TTree *dst_tree = dynamic_cast<TTree*>(dst))
      if (TTree *src_tree = dynamic_cast<TTree*>(src))
         return merge(dst_tree, src_tree);

   fprintf(stderr, "TH1 -> ");
   if (TH1 *dst_hist = dynamic_cast<TH1*>(dst))
      if (TH1 *src_hist = dynamic_cast<TH1*>(src))
         return merge(dst_hist, src_hist);

   fprintf(stderr, "TDirectory -> ");
   if (TDirectory *dst_dir = dynamic_cast<TDirectory*>(dst))
      return merge(dst_dir, src);

   return nullptr;
}

void fill(TTree* t)
{
   int n;

   t->Branch("n", &n);

   for (auto i : {0, 1, 2, 3, 4, 5})
      n = i, t->Fill();

   t->ResetBranchAddresses();
}

int main()
{
   std::vector<void*> tmp(0UL);

   TFile f("root-merge.root", "recreate");

   TTree* t1 = new TTree("t1", "t1");
   TTree* t2 = new TTree("t2", "t2");

   fill(t1); t1->Scan("n");
   fill(t2); t2->Scan("n");

   if (auto t = (TTree*) merge((TObject*)t1, (TObject*)t2))
      t->Scan("n");

   return 0;
}
