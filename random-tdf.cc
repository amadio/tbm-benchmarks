#include <cstdlib>
#include <random>

#include "ROOT/RDataFrame.hxx"
#include "TTree.h"

int main(int argc, char **argv)
{
   gROOT->SetBatch();

   size_t nWorkers = 1;
   size_t nEntries = 128*1024*1024;
   size_t nDiscards = 0;

   if (argc >= 2) nWorkers    = std::atoi(argv[1]);
   if (argc >= 3) nEntries    = std::atoi(argv[2]);
   if (argc >= 4) nDiscards   = std::atoi(argv[3]);

   if (nWorkers > 1)
      ROOT::EnableImplicitMT(nWorkers);

   ROOT::RDataFrame tdf(nEntries);

   auto random = [=]() {
      static thread_local std::default_random_engine g;
      static thread_local std::uniform_real_distribution<double> dist(0.0, 1.0);

      double result;

      for (size_t i = 0; i <= nDiscards; ++i)
        result = dist(g);

      return result;
   };

   tdf.Define("random", random)
      .Snapshot<double>("tree", "random.root", { "random" });

   return 0;
}
