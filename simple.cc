#include <cstdlib>

#include "ROOT/TDataFrame.hxx"
#include "TTree.h"

int main(int argc, char **argv)
{
   gROOT->SetBatch();

   size_t nWorkers = 2;
   size_t nEntries = 16*1024*1024;

   if (argc >= 2) nWorkers    = std::atoi(argv[1]);
   if (argc >= 3) nEntries    = std::atoi(argv[2]);

   if (nWorkers > 1)
      ROOT::EnableImplicitMT(nWorkers);

   ROOT::Experimental::TDataFrame tdf(nEntries);

   auto counter = []() {
      static thread_local unsigned int count = 0;
      auto retval = ++count;
      return retval;
   };

   tdf.Define("counter", counter)
      .Snapshot<unsigned int>("tree", "simple.root", { "counter" });

   return 0;
}
