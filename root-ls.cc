#include <iostream>

#include "TFile.h"
#include "TKey.h"
#include "TTree.h"

void indent(int depth)
{
    for (int i = 0; i < depth; ++i)
        printf(" ");
}

void list(TKey*, int);
void list(TObject*, int);
void list(TDirectory*, int);

void list(TKey *key, int depth)
{
    indent(depth);
    printf("TKey: %s:%s;%d\n", key->GetClassName(), key->GetName(), (int)key->GetCycle());

    if (TObject* obj = key->ReadObj()) {
       if (strncmp("TDirectory", key->GetClassName(), 10) == 0) {
            if (TDirectory *dir = dynamic_cast<TDirectory*>(obj))
                list(dir, depth+1);
       } else if (TTree *t = dynamic_cast<TTree*>(obj)) {
            list(t, depth+1);
       }
    }
}

void list(TObject *obj, int depth)
{
    indent(depth);
    printf("%s: %s\n", obj->ClassName(), obj->GetName());

    if (TDirectory *dir = dynamic_cast<TDirectory*>(obj))
        list(dir, depth+1);
}

void list(TDirectory *dir, int depth)
{
    for (TObject* obj : *(dir->GetList()))
        list(obj, depth+1);

    for (TObject* obj : *(dir->GetListOfKeys()))
        if (TKey *key = dynamic_cast<TKey*>(obj))
            list(key, depth+1);
}

int main(int argc, char **argv)
{
    for (int i = 1; i < argc; ++i) {
        TFile f(argv[i], "read");

        if (f.IsZombie()) {
            fprintf(stderr, "%s: error opening file: %s\n", basename(argv[0]), argv[i]);
            continue;
        }

        printf("TFile: %s\n", f.GetName());

        for (TObject* obj : *(f.GetListOfKeys()))
            if (TKey *key = dynamic_cast<TKey*>(obj))
                list(key, 1);

        printf("\n");
    }

    return 0;
}
